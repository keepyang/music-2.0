// api文件夹下 各个请求模块js, 都统一来到index.js再向外导出
import { recommendMusic, newMusic, hitSong } from './Home'
import { albumArt, albumArtists, artist, follow, hotSearch, multimatch, searchResultList, single, suggeations } from './Search'
import { getSongById, getLyricById, RelatedPlaylistById, SimilarSongsById } from './Play'
import { albumcommentsById, comments, songcommentsById, userinfoById, usersongallById, usersonglistById } from './Comments'

export const recommendMusicAPI = recommendMusic // 请求推荐歌单的方法导出
export const newMusicAPI = newMusic // 首页 - 最新音乐
export const hitSongAPI = hitSong // 热歌榜

export const hotSearchAPI = hotSearch // 搜索 - 热搜关键词
export const searchResultListAPI = searchResultList // 搜索 = 搜索结果
export const albumArtAPI = albumArt // 搜索 = 搜索结果 - 专辑
export const suggeationsAPI = suggeations // 搜索 = 搜索建议
export const multimatchAPI = multimatch // 搜索 = 搜索多重匹配

export const getSongByIdAPI = getSongById // 歌曲 - 播放地址
export const getLyricByIdAPI = getLyricById // 歌曲 - 歌词数据

export const RelatedPlaylistByIdAPI = RelatedPlaylistById // 播放页 - 相关歌单
export const SimilarSongsByIdAPI = SimilarSongsById // 播放页 - 相似歌曲

export const commentsAPI = comments // 歌单 - 评论
export const songCommentsAPI = songcommentsById // 歌曲 - 评论
export const albumCommentsAPI = albumcommentsById // 专辑 - 评论
export const userInfoAPI = userinfoById // 评论 - 用户信息
export const userSongListAPI = usersonglistById // 评论 - 用户歌单

export const userSongAllAPI = usersongallById // 用户 - 歌单所有歌曲

export const singleAPI = single // 歌手 - 歌曲信息
export const artistAPI = artist // 歌手 - 歌手信息
export const followAPI = follow // 歌手 - 歌手粉丝数量
export const albumArtistsAPI = albumArtists // 歌手 - 歌手专辑


