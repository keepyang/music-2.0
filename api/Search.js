// 搜索模块
import request from '@/utils/request'

// 热搜关键字
export const hotSearch = (params) =>
  request({
    url: '/search/hot',
    params,
  })

// 搜索结果
export const searchResultList = (params) =>
  request({
    url: '/cloudsearch',
    params,
  })

// 搜索多重匹配
export const multimatch = (params) =>
  request({
    url: '/search/multimatch',
    params,
  })

//获取专辑内容
export const albumArt = (id) =>
  request({
    url: `/album?id=${id}`,
    method: 'GET',
  })

//获取歌曲信息
export const single = (id) =>
  request({
    url: `/artists?id=${id}`,
    method: 'GET',
  })

//获取歌手信息
export const artist = (id) =>
  request({
    url: `/artist/detail?id=${id}`,
    method: 'GET',
  })

//获取歌手粉丝数量
export const follow = (id) =>
  request({
    url: `/artist/follow/count?id=${id}`,
    method: 'GET',
  })

//获取歌手专辑
export const albumArtists = (id) =>
  request({
    url: `/artist/album?id=${id}&limit=1`,
    method: 'GET',
  })

//获取搜索建议
export const suggeations = (keywords) =>
  request({
    url: `/search/suggest?keywords=${keywords}&type=mobile`,
    method: 'GET',
  })
