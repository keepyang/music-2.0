//评论模块
import request from '@/utils/request'

//歌单评论
export const comments = (params) =>
  request({
    url: '/comment/playlist',
    params,
  })

//歌曲评论
export const songcommentsById = (params) =>
  request({
    url: '/comment/music',
    params,
  })

//专辑评论
export const albumcommentsById = (params) =>
  request({
    url: '/comment/album',
    params,
  })

//评论区的用户信息
export const userinfoById = (uid) =>
  request({
    url: `/user/detail?uid=${uid}`,
    method: 'GET',
  })

//获取用户歌单
export const usersonglistById = (params) =>
  request({
    url: '/user/playlist',
    params,
  })

//获取用户歌单
export const usersongallById = (params) =>
  request({
    url: '/playlist/track/all',
    params,
  })
