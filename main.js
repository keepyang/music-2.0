import Vue from 'vue'
import App from './App'
import plugins from './plugins' // plugins
Vue.use(plugins)

Vue.config.productionTip = false

import uView from '@/uni_modules/uview-ui'
Vue.use(uView)

import share from './utils/share.js'	// 导入并挂载全局的分享方法
Vue.mixin(share)

App.mpType = 'app'

const app = new Vue({
  ...App
})

app.$mount()
