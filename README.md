#### 项目:`网易云音乐 微信小程序`

<!-- ###### APP 下载 -->

<!-- <img src="./wyy.png"/> -->

##### 项目启动时间:2023/08/23

##### 技术栈

uniapp + uview2.0 + ES6/7 + scss

##### 项目运行

```
git clone https://gitee.com/keepyang/music-2.0.git

```

##### 项目描述

> 网易云音乐是一款专注于发现与分享的音乐产品,依托专业音乐人、DJ、好友推荐及社交功能,为用户打造全新的音乐生活。

##### 主要模块

- [x] 首页模块 `Home.vue` -- 完成
- [x] 专辑页面 `AlbumArt.vue` -- 完成
- [x] 歌曲播放页面 `Play.vue` -- 完成
- [x] 歌手信息页面 `SingerDetails.vue` -- 完成
- [x] 歌单页面 `SongList.vue` -- 完成
- [x] 用户页面进入歌单页面--显示所有歌曲 `SongLists.vue` -- 完成
- [x] 用户信息页面 `UserInfo.vue` -- 完成
- 组件
- [x] 搜索回车显示的歌手/专辑/歌单组件 `AlbumView.vue` -- 完成
- [x] 首页卡片组件 `CardView.vue` -- 完成
- [x] 评论区组件 `CommentView.vue` -- 完成
- [x] 抽屉组件 `DrawerView.vue` -- 完成
- [x] 热歌榜 `HitSongList.vue` -- 完成
- [x] 歌曲列表组件 `MusicList.vue` -- 完成
- [x] 搜索页面歌曲的展示组件 `SearchSongsList.vue` -- 完成
- [x] 搜索页面组件 `SearchView.vue` -- 完成
- [x] 用户页面 音乐品味(喜欢的音乐) `UserCard.vue` -- 完成
- [x] 用户页面 收藏的歌单 `UserCards.vue` -- 完成

##### 项目完成时间:2023/09/02

##### 后续优化
>1.抽屉组件tab选项锚点匹配的问题 已解决
>2.抽屉组件可以拖拽打开和关闭

## `END`