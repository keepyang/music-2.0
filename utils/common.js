/**
* 显示消息提示框
* @param content 提示的标题
*/
export function toast(content) {
  uni.showToast({
    icon: 'none',
    title: content
  })
}

/**
* 显示模态弹窗
* @param content 提示的标题
*/
export function showConfirm(content) {
  return new Promise((resolve, reject) => {
    uni.showModal({
      title: '提示',
      content: content,
      cancelText: '取消',
      confirmText: '确定',
      success: function(res) {
        resolve(res)
      }
    })
  })
}

/**
* 参数处理
* @param params 参数
*/
export function tansParams(params) {
  let result = ''
  for (const propName of Object.keys(params)) {
    const value = params[propName]
    var part = encodeURIComponent(propName) + "="
    if (value !== null && value !== "" && typeof (value) !== "undefined") {
      if (typeof value === 'object') {
        for (const key of Object.keys(value)) {
          if (value[key] !== null && value[key] !== "" && typeof (value[key]) !== 'undefined') {
            let params = propName + '[' + key + ']'
            var subPart = encodeURIComponent(params) + "="
            result += subPart + encodeURIComponent(value[key]) + "&"
          }
        }
      } else {
        result += part + encodeURIComponent(value) + "&"
      }
    }
  }
  return result
}

/**
 * 数字格式化：万、亿单位
 * @param {*} val
 * @returns
 */
export function numberFormat(val) {
  let num = 10000
  let sizesValue = ''
  if (val < 10000) {
    sizesValue = ''
    return val
  } else if (val > 10000 && val < 99999999) {
    sizesValue = '万'
  } else if (val > 100000000) {
    sizesValue = '亿'
  }
  let i = Math.floor(Math.log(val) / Math.log(num))
  let sizes = (val / Math.pow(num, i)).toFixed(1)
  sizes = sizes + sizesValue
  return sizes // 输出
}

/**
 * 当前日期
 * @returns 'MM月dd日'
 */
export function currentDate(date = new Date()) {
  date = new Date(date)
  let month = date.getMonth() + 1
  let day = date.getDate()
  return month + '月' + day + '日'
}

/**
 * 获取距离当前时间某天数的日期
 * @days 天数 值：0=今天;1=明天;-1=昨天...
 * @return 格式：yyyy-MM-dd
 */
export function getRecentDay_Date(days) {
  let dd = new Date()
  dd.setDate(dd.getDate() + days) //获取days天后的日期
  let y = dd.getFullYear()
  let m = dd.getMonth() + 1 //获取当前月份的日期
  let d = dd.getDate()
  let day = y + '-' + m + '-' + d
  return day
}

/**
 * 时间戳转 时间 年-月-日
 * @param 1586275200000
 * @returns 格式：yyyy-MM-dd
 */
export function filterTime(time) {
  const date = new Date(time)
  const Y = date.getFullYear()
  const M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
  const D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
  return `${Y}-${M}-${D}`
}

/**
 * 时间字符串转 时间 年-月-日
 * @str 2202-23-23
 * @returns 格式：2202年23月23日
 */
export function ConversionTime(str) {
  let arr = str.split('-')
  if (arr.length === 3) {
    return arr[0] + '年' + arr[1] + '月' + arr[2] + '日'
  } else if (arr.length === 2) {
    return new Date().getFullYear() + '年' + arr[0] + '月' + arr[1] + '日'
  } else {
    return str
  }
}
