/**
 * 名字格式化：名字之间加 "/"
 * @arr 名字集成的数组
 * @returns
 */
export function singer() {
	return (arr) => {
		let str = arr[0].name
		if (arr.length > 1) {
			for (let i = 1; i < arr.length; i++) {
				str += ' / ' + arr[i].name
			}
			return str
		} else {
			return str
		}
	}
}

/**
 * 歌曲播放限制：
 * @id 歌曲id值
 * @fee 判断歌曲是否播放的值
 */
export function playSong(id, fee) {
	if (fee == 8) {
		uni.navigateTo({
			url: '/pages/Play?id=' + id
		});
	} else if (fee == 4) {
		this.$refs.uToast.show({
			type: 'error',
			icon: false,
			message: "唱片公司要求该歌曲须付费,购买数字专辑后即可畅听~"
		})
	} else if (fee == 1) {
		this.$refs.uToast.show({
			type: 'error',
			icon: false,
			message: "当前歌曲为VIP歌曲,购买VIP服务后即可畅听~"
		})
	} else {
		this.$refs.uToast.show({
			type: 'error',
			icon: false,
			message: "由于版权保护,您所在的地区暂时无法使用"
		})
	}
}