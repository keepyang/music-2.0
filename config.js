// 应用全局配置
module.exports = {
	
  baseUrl: 'https://web.codeboy.com/music',
	// #ifdef H5
	baseUrl: '/music',
	// #endif
	
  // 应用信息
  appInfo: {
    // 应用名称
    name: "云音乐",
    // 应用版本
    version: "1.1.0",
    // 应用logo
    logo: "https://raw.githubusercontent.com/keepyanghao/images/main/music/logo.png",
    // 官方网站
    site_url: "http://ruoyi.vip",
    // 政策协议
    agreements: [{
        title: "隐私政策",
        url: "https://ruoyi.vip/protocol.html"
      },
      {
        title: "用户服务协议",
        url: "https://ruoyi.vip/protocol.html"
      }
    ]
  }
}
